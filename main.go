package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"
	"projects/social-go/internal/handler"
	"projects/social-go/internal/service"

	"github.com/hako/branca"
	"github.com/joho/godotenv"

	// this is postgres drivers
	_ "github.com/jackc/pgx/stdlib"
)

var (
	port          string
	dataBasebName string
	localHost     string
	user          string
	url           string
	password      string
)

func init() {
	if err := godotenv.Load("config.env"); err != nil {
		panic(err)
	}

	if _port := os.Getenv("PORT"); len(_port) > 0 {
		port = _port
	}

	if _dbName := os.Getenv("DATABASENAME"); len(_dbName) > 0 {
		dataBasebName = _dbName
	}

	if _user := os.Getenv("USER"); len(_user) > 0 {
		user = _user
	}

	if _url := os.Getenv("URL"); len(_url) > 0 {
		url = _url
	}

	if _password := os.Getenv("PASSWORD"); len(_password) > 0 {
		password = _password
	}

	if _localHost := os.Getenv("LOCALHOST"); len(_localHost) > 0 {
		localHost = _localHost
	}
}

func main() {
	// db := db.DataBase{}
	// db.Mirgation(dataBasebName, user, url, password)
	// db.Connect(dataBasebName, user, url, password)

	dns := "postgres://" + user + ":" + password + "@" + url + ":5432/" + dataBasebName
	db, err := sql.Open("pgx", dns)
	if err != nil {
		panic("CHANGE PARAMS FOR CONNECT DATABASE (url, password and more...)")
	}

	defer db.Close()
	err = db.Ping()
	if err != nil {
		log.Printf("DB:-->::Connect::ERROR DB PING:%s\n", err)
		panic(err)
	}

	log.Printf("DB:-->::Connect\n")
	codec := branca.NewBranca("supersecretkeyyoushouldnotcommit")

	s := service.New(db, codec)
	h := handler.New(s)

	log.Printf("accepting connections on port %s\n", port)
	if err = http.ListenAndServe(":"+port, h); err != nil {
		log.Fatalf("could not start server: %v\n", err)
	}
}
