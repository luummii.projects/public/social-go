package db

import (
	"database/sql"

	"log"

	"github.com/gobuffalo/packr"
	"github.com/rubenv/sql-migrate"

	// this is postgres drivers
	_ "github.com/jackc/pgx"
)

// DataBase -
type DataBase struct {
}

// Connect datadase
func (*DataBase) Connect(database, user, url, password string) {
	dsn := "postgres://" + user + ":" + password + "@" + url + ":5432/" + database
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		panic("CHANGE PARAMS FOR CONNECT DATABASE (url, password and more...)")
	}

	defer db.Close()
	err = db.Ping()
	if err != nil {
		log.Printf("DB:-->::Connect::ERROR DB PING:%s\n", err)
		panic(err)
	}

	log.Printf("DB:-->::Connect\n")
}

// Mirgation datadase
func (*DataBase) Mirgation(database, user, url, password string) {
	dsn := "postgres://" + user + ":" + password + "@" + url + ":5432/" + database
	db, err := sql.Open("postgres", dsn)
	migrations := &migrate.PackrMigrationSource{
		Box: packr.NewBox("./migrations"),
	}
	n, err := migrate.Exec(db, "postgres", migrations, migrate.Up)
	if err != nil {
		log.Printf("Mirgation is error: %s", err)
	}
	log.Printf("Applied %d migrations!\n", n)
}
